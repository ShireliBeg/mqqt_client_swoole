<?php
/**
 * Created by PhpStorm.
 * User: evenvi
 * Date: 16-8-10
 * Time: 下午3:37
 */

namespace Evenvi\Mqtt;


interface MqttClientInterface
{
    public function connect($cli, $client_id);
    public function pub($topic, $content);
    public function sub($topics);
}