<?php
/**
 * Created by PhpStorm.
 * User: evenvi
 * Date: 16-8-11
 * Time: 上午10:04
 */

define("MQTT_CONNECT", 1);
define("MQTT_CONNACK", 2);
define("MQTT_PUBLISH", 3);
define("MQTT_PUBACK", 4);
define("MQTT_PUBREC", 5);
define("MQTT_PUBREL", 6);
define("MQTT_PUBCOMP", 7);
define("MQTT_SUBSCRIBE", 8);
define("MQTT_SUBACK", 9);
define("MQTT_UNSUBSCRIBE", 10);
define("MQTT_UNSUBACK", 11);
define("MQTT_PINGREQ", 12);
define("MQTT_PINGRESP", 13);
define("MQTT_DISCONNECT", 14);